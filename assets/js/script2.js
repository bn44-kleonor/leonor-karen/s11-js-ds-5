 /* =========================================== 
      ACTIVITY 3: Create a shift method that will REMOVE the first node from the list. 
      It should display "List is currently empty" in the console and return undefined. 
  ==============================================*/

/* =========================================== 
      ACTIVITY 4: Create an edit method that will EDIT/UPDATE the value of a given node. 
      It should accept 2 parameters: value and newValue.
  
      Stretch Goal: If the value does not exist, it should display 
      "The given value is not in the list" in the console and return undefined.
==============================================*/

/* =========================================== 
      ACTIVITY 5 (STRETCH GOAL)
      Create a length property with an initial value of 0
      Create a size method that returns the correct length of the list. 
      
      Note: The length should be updated push, unshift, shift, and remove methods are called.
==============================================*/


// /*=====================================

// DISCUSSION / ACTIVITY 
// WD004-S10-DATA-STRUCTURES-5

// LINKED LIST
// A linear data structure where each element (aka __NODE__) is a separate __OBJECT__.
// Each node has 2 properties: a ___VALUE___and a ___NEXT___ pointer.                     //value carries the data, next points to the next node
// ____VALUE___holds data while ____NEXT____is a pointer/reference to the next node. 


// The entry point into a linked list is the __HEAD___(1st pointer). The head is not a separate node but a pointer/reference to the __FIRST__node.
// If the list is empty, then head is a ___NULL___reference.

// The last node into a linked list has its next pointer set to ___NULL___.

// Like an array, a linked list can contain different data types. 
// However, unlike array, a linked list has ___NO___indexing. 
// You have to ___TRAVERSE___the list from the head, in order, until you get to the node that you are looking for (i.e., value).

// If a node has no next pointer pointing to it, it is ___ORPHANED___and removed from the list.


// ======================================*/

// function LinkedList(){
//    let Node = function(value){
//          this.value = value;
//          this.next = null;
//    }

//    this.head = null;

//    this.getHead = function(){
//          return this.head;
//    }

//    //push - add a new node to the end
//    this.push = function(value){
//          let node = new Node(value);

//          //if the list is empty
//          if(this.head == null) {
//                this.head = node;
//          } else {
//    /*===============================
//     ACTIVITY 1: Write a code to add a node at the end if the list is NOT empty

//     Note: 
//     You need to declare a currentNode variable that will be used to traverse the LinkedList from its head.
   
//     =================================*/
//                let currentNode = this.head;

//                while(currentNode.next _____ null) {
//                      // move the currentNode to the next node
//                }

//                /*once you have reached the LAST node, 
//                point the next pointer of the currentNode to the
//                 new node that you just created */
//          }
//    }


//    /*===============================
//     ACTIVITY 2: Create an unshift method which adds a node to the start of the linked list
//   =================================*/
  
// }

// let pets = new LinkedList();
// pets.push('kitten');
// pets.push('puppy');
// pets.push('goldfish');
// pets.push('hamster');
// // pets.unshift('wolf');
// // console.log(pets.getHead()); //kitten
// console.log(pets);




/*====== start of discussion ==================================================================*/


function LinkedList() {
      //constructor function to instantiate nodes
      function Node(value) {
            this.value = value;                       // to create properties/keys of the object user "this"
            this.next = null;                   // the object is the 'Node'
            //this.length = 0;
      }

      this.length = 0;
      //let length = 0;
      //head is a pointer, not a node
      this.head = null;                         // a property/key of LinkedList

      //***PUSH
      //ADDS A NEW NODE AT THE END
      this.push = function(value){
            //return console.log(value);
            let node = new Node(value);         //created a node object that is separated for now to the 'LinkedList', we will put this indes the 'LinkedList' object inside head.
            //return console.log(node);
      
            //this.head = node;                       // this will insert the 'node' object to the 'Linked List' at the 'head' position.

            if(this.head === null){             // if LinkedList is empty its head si null at first. so the first push is on head
                  this.head = node;
                  //this.length = this.length + 1;      // add value of length since a value was pushed
            } else {
                  //return console.log("test");
                  let currentNode = this.head   //basta may laman na ang head (yung first push) //kim
                  //return console.log(currentNode);
                  //return console.log(node);         // laman ng sumunod na ni-push (or current na ni-pupush) //e.g. maja

                  //to do: traverse the elements being pushed (what we know: we know that the next (the last) key is always 'null')
                  // tinitignan nya if ang LinkedList objects/subojects/nestedObjects ay may laman, kung wala tsaka nya ipapasok yung new node na nipupush  | gGirls.push("bea");
                  while(currentNode.next !== null){
                        currentNode = currentNode.next      //read right to left
                  }
                  currentNode.next = node             //assigned the currentNode.next sa new node na ginawa (yung bagong push)
                  //this.length = this.length + 1;      // add value of length since a value was pushed
            }
      }

      //***UNSHIFT
      //ADD A NODE TO THE START
      this.unshift = function(value) {
            let node = new Node(value)          //instantiate kasi may ipapasa na value
            //return console.log(this.head.value);
            //return console.log(node)    //pia

            //and nasa head si kim, so si Pia idadagdag before sa node kung saan ang value ay si Kim
            // so ang node next dapat ay ang head na si Kim, thus below 
            node.next = this.head;  // so kay node na ang laman si pia, lalagyan mo ng value ang 'next' property nya ng current head na si kim
            this.head = node;       // ang bagong head na dapat si pia (which is currently laman ni 'node')
            //this.length = this.length + 1;      // add value of length since a value was pushed
      }

      //***REMOVE
      //REMOVE THE NODE WITH THE PASSED IN ARGUMENT
      this.remove = function(val){
            let currentNode = this.head;
            let previousNode;
            //return console.log(currentNode.value);  //Pia
            if(currentNode.value === val) {
                  //console.log(currentNode.next);          //kim
                  //return console.log("sya nga!");
                  this.head = currentNode.next;             //ibabalik si kim sa 'this.head'
                  //this.length = this.length - 1;   // subtract value of length since a value was pushed
            } else {
                  //return console.log("hindi sya!");
                  //this.head = currentNode.next;
                  //return console.log(currentNode);              //kim
                  //return console.log(currentNode.next;);        //maja
                  //console.log(currentNode.next.next)            //bea
                  while(currentNode.value !== val ){
                        previousNode = currentNode;               //assign currentNode (kim) to temp previousNode
                        currentNode = currentNode.next;           // modify control variable 'currentNode.value'
                        // //currentNode.next = currentNode.next.next
                        // currentNode.next = null;
                        //currentNode.next
                        //currentNode.next = currentNode.next.next;
                  }
                        //return console.log(previousNode);       //kim
                        //return console.log(previousNode.next);  //maja
                        //return console.log(currentNode);        //maja
                        //return console.log(currentNode.next);   //bea
                        previousNode.next = currentNode.next;
                        //this.length = this.length - 1;  // subtract value of length since a value was pushed
            }

      }


      //***SHIFT
 /* =========================================== REMOVE
      ACTIVITY 3: Create a shift method that will REMOVE the first node from the list. 
      It should display "List is currently empty" in the console and return undefined. 
  ==============================================*/
      this.shift = function(){
            if(this.head === null) {
                  console.log("List is currently empty");
                  return undefined;
            } else {
                  this.head = this.head.next;   //the head node object (kim) will be replaced by the value of it's 'next' (which is inside this.head.next)
                  //this.length = this.length - 1;       // subtract value of length since a value was pushed
            }
      }


/* =========================================== EDIT/UPDATE
      ACTIVITY 4: Create an edit method that will EDIT/UPDATE the value of a given node. 
      It should accept 2 parameters: value and newValue.
  
      Stretch Goal: If the value does not exist, it should display console.log("The given value is not in the list");he given value is not in the list" in the console and return undefined.
==============================================*/
      this.update = function(val, newVal){
            let currentNode = this.head;
            let previousNode;
            if(currentNode.value === val) {
                  this.head.value = newVal;
            } else {
                  while(currentNode.value !== val || currentNode.next === null){
                             // modify control variable 'currentNode.value'
                        if(currentNode.next === null) {
                              console.log("The given value is not in the list");
                        }
                        currentNode = currentNode.next;
                  }
                        currentNode.value = newVal;
            }
      }


      //***SIZE
/* =========================================== 
      ACTIVITY 5 (STRETCH GOAL)
      Create a length property with an initial value of 0
      Create a size method that returns the correct length of the list. 
      
      Note: The length should be updated push, unshift, shift, and remove methods are called.
==============================================*/
      this.size = function(){
            let currentNode = this.head;
            //console.log("size - currentNode below");
            //console.log(currentNode);
            while(currentNode.next !== null ){
                  currentNode = currentNode.next;     // modify control variable 'currentNode.value'
                  this.length++
            }
            //return console.log(this.length+1);
            return this.length+1;
      }
}



//instantiate
let gGirls = new LinkedList();
gGirls.push("kim");
gGirls.push("maja");
gGirls.push("bea");
gGirls.push("a");
gGirls.push("b");
gGirls.push("c");
console.log(gGirls.shift());
//gGirls.unshift("pia");
//gGirls.remove("pia");
//gGirls.remove("maja");
gGirls.update("maja", "sarah");
console.log(gGirls);

gGirls.update("abc", "sarah");
console.log(gGirls);

//console.log("length = " + gGirls.size());
//console.log(gGirls);
