/*=====================================

DISCUSSION / ACTIVITY 
WD004-S10-DATA-STRUCTURES-5

LINKED LIST
A linear data structure where each element (aka __NODE__) is a separate __OBJECT__.
Each node has 2 properties: a ___VALUE___and a ___NEXT___ pointer.				//value carries the data, next points to the next node
____VALUE___holds data while ____NEXT____is a pointer/reference to the next node. 


The entry point into a linked list is the __HEAD___(1st pointer). The head is not a separate node but a pointer/reference to the __FIRST__node.
If the list is empty, then head is a ___NULL___reference.

The last node into a linked list has its next pointer set to ___NULL___.

Like an array, a linked list can contain different data types. 
However, unlike array, a linked list has ___NO___indexing. 
You have to ___TRAVERSE___the list from the head, in order, until you get to the node that you are looking for (i.e., value).

If a node has no next pointer pointing to it, it is ___ORPHANED___and removed from the list.


======================================*/

function LinkedList(){
	let Node = function(value){
		this.value = value;
		this.next = null;
	}

	this.head = null;

	this.getHead = function(){
		return this.head;
	}

	//push - add a new node to the end
	this.push = function(value){
		let node = new Node(value)				//instantiate a node before pushing value

		console.log("value of intantiated node is below");
		console.log(node);
		console.log(" ");



		//if the list is empty/null (meaning the first push is the head)
		if(this.head == null) {
			this.head = node;

			console.log("value of this.head is below");
			console.log(this.head);
			console.log(" ");

		} else {

			let currentNode = this.head;

			console.log("value of currentNode is below from this.head");
			console.log(currentNode);
			console.log(" ");

			while(currentNode.next != null) {
				// move the currentNode to the next node
				//currentNode.next = node;

				console.log("inside WHILE value of currentNode.NEXT is below");
				console.log(currentNode);
				console.log(" ");

				currentNode = currentNode.next;

				console.log("inside WHILE value of currentNode is below (from currentNode.next)");
				console.log(currentNode);
				console.log(" ");

			}

			//once you have reached the LAST node, point the next pointer of the currentNode to the
			//new node that you just created
			currentNode.next = node;

		}
		return node;	//console.log(node);

		this.push(currentNode) = this.next;
		console.log("******** what???!!!!!!!**********")

	}
}


let pets = new LinkedList();
//1st
console.log(" ")
console.log("START of first PUSH kitten")
pets.push('kitten');
console.log("pets.getHead() below")
console.log(pets.getHead());	//kitten
console.log(" ")

console.log("pets linked list below")
console.log(pets);
console.log("************* end of PUSH kitten")
console.log(" ")



//2nd
console.log(" ")
console.log("START of PUSH puppy")
pets.push('puppy');
console.log("pets.getHead() below")
console.log(pets.getHead());	//kitten
console.log(" ")


console.log("pets linked list below")
console.log(pets);
console.log("************* end of PUSH puppy")
console.log(" ")



//3rd
console.log(" ")
console.log("START of PUSH goldfish")
pets.push('goldfish');
console.log("pets.getHead() below")
console.log(pets.getHead());	//kitten
console.log(" ")

console.log("pets linked list below")
console.log(pets);
console.log("************* end of PUSH goldfish")
console.log(" ")



//4th
console.log(" ")
console.log("START of PUSH hamster")
pets.push('hamster');
console.log("pets.getHead() below")
console.log(pets.getHead());	//kitten
console.log(" ")

console.log("pets linked list below")
console.log(pets);
console.log("************* end of PUSH hamster")
console.log(" ")

