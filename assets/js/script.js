// /*=====================================

// DISCUSSION / ACTIVITY 
// WD004-S10-DATA-STRUCTURES-5

// LINKED LIST
// A linear data structure where each element (aka __NODE__) is a separate __OBJECT__.
// Each node has 2 properties: a ___VALUE___and a ___NEXT___ pointer.				//value carries the data, next points to the next node
// ____VALUE___holds data while ____NEXT____is a pointer/reference to the next node. 


// The entry point into a linked list is the __HEAD___(1st pointer). The head is not a separate node but a pointer/reference to the __FIRST__node.
// If the list is empty, then head is a ___NULL___reference.

// The last node into a linked list has its next pointer set to ___NULL___.

// Like an array, a linked list can contain different data types. 
// However, unlike array, a linked list has ___NO___indexing. 
// You have to ___TRAVERSE___the list from the head, in order, until you get to the node that you are looking for (i.e., value).

// If a node has no next pointer pointing to it, it is ___ORPHANED___and removed from the list.


// ======================================*/

// function LinkedList(){
// 	let Node = function(value){
// 		this.value = value;
// 		this.next = null;
// 	}

// 	this.head = null;

// 	this.getHead = function(){
// 		return this.head;
// 	}

// 	//push - add a new node to the end
// 	this.push = function(value){
// 		let node = new Node(value);

// 		//if the list is empty
// 		if(this.head == null) {
// 			this.head = node;
// 		} else {
// 	/*===============================
//     ACTIVITY 1: Write a code to add a node at the end if the list is NOT empty

//     Note: 
//     You need to declare a currentNode variable that will be used to traverse the LinkedList from its head.
   
//     =================================*/
//     		let currentNode = this.head;

//     		while(currentNode.next _____ null) {
//     			// move the currentNode to the next node
//     		}

//     		/*once you have reached the LAST node, 
//     		point the next pointer of the currentNode to the
//     		 new node that you just created */
// 		}
// 	}


// 	/*===============================
//     ACTIVITY 2: Create an unshift method which adds a node to the start of the linked list
//   =================================*/
  
// }

// let pets = new LinkedList();
// pets.push('kitten');
// pets.push('puppy');
// pets.push('goldfish');
// pets.push('hamster');
// // pets.unshift('wolf');
// // console.log(pets.getHead()); //kitten
// console.log(pets);




/*====== start of discussion ==================================================================*/


function LinkedList() {
	//constructor function to instantiate nodes
	function Node(value) {
		this.value = value;				// to create properties/keys of the object user "this"
		this.next = null;				// the object is the 'Node'
	}

	//head is a pointer, not a node
	this.head = null;					// a property/key of LinkedList

	//ADDS A NEW NODE AT THE END
	this.push = function(value){
		//return console.log(value);
		let node = new Node(value);		//created a node object that is separated for now to the 'LinkedList', we will put this indes the 'LinkedList' object inside head.
		//return console.log(node);
	
		//this.head = node;				// this will insert the 'node' object to the 'Linked List' at the 'head' position.

		if(this.head === null){			// if LinkedList is empty its head si null at first. so the first push is on head
			this.head = node;
		} else {
			//return console.log("test");
			let currentNode = this.head 	//basta may laman na ang head (yung first push) //kim
			//return console.log(currentNode);
			//return console.log(node);		// laman ng sumunod na ni-push (or current na ni-pupush) //e.g. maja

			//to do: traverse the elements being pushed (what we know: we know that the next (the last) key is always 'null')
			// tinitignan nya if ang LinkedList objects/subojects/nestedObjects ay may laman, kung wala tsaka nya ipapasok yung new node na nipupush  | gGirls.push("bea");
			while(currentNode.next !== null){
				currentNode = currentNode.next   	//read right to left
			}
			currentNode.next = node  		//assigned the currentNode.next sa new node na ginawa (yung bagong push)
		}
	}


	//ADD A NODE TO THE START
	this.unshift = function(value) {
		let node = new Node(value)		//instantiate kasi may ipapasa na value
		//return console.log(this.head.value);
		//return console.log(node)	//pia

		//and nasa head si kim, so si Pia idadagdag before sa node kung saan ang value ay si Kim
		// so ang node next dapat ay ang head na si Kim, thus below 
		node.next = this.head;	// so kay node na ang laman si pia, lalagyan mo ng value ang 'next' property nya ng current head na si kim
		this.head = node;		// ang bagong head na dapat si pia (which is currently laman ni 'node')
	}


	//REMOVE THE NODE WITH THE PASSED IN ARGUMENT
	this.remove = function(val){
		let currentNode = this.head;
		let previousNode;
		//return console.log(currentNode.value);	//Pia
		if(currentNode.value === val) {
			//console.log(currentNode.next);		//kim
			//return console.log("sya nga!");
			this.head = currentNode.next;			//ibabalik si kim sa 'this.head'
		} else {
			//return console.log("hindi sya!");
			//this.head = currentNode.next;
			//return console.log(currentNode);			//kim
			//return console.log(currentNode.next;);		//maja
			//console.log(currentNode.next.next)		//bea
			while(currentNode.value !== val ){
				previousNode = currentNode;			//assign currentNode (kim) to temp previousNode
				currentNode = currentNode.next;		// modify control variable 'currentNode.value'
				// //currentNode.next = currentNode.next.next
				// currentNode.next = null;
				//currentNode.next
				//currentNode.next = currentNode.next.next;
			}
				//return console.log(previousNode);		//kim
				//return console.log(previousNode.next);	//maja
				//return console.log(currentNode);		//maja
				//return console.log(currentNode.next);	//bea
				previousNode.next = currentNode.next;
		}

	}

}


//instantiate
let gGirls = new LinkedList();
gGirls.push("kim");
gGirls.push("maja");
gGirls.push("bea");
gGirls.unshift("pia");
//gGirls.remove("pia");
//gGirls.remove("maja");
console.log(gGirls);


